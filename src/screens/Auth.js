import React, { Component } from 'react';
import {
    ImageBackground,
    Text,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    StyleSheet,
    Alert
} from 'react-native';
import { server, showError, showSuccess } from '../global';
import axios from 'axios';
import BackgroundImage from '../../assets/imgs/login.jpg';
import GlobalStyle from '../common';
import AuthInput from '../components/AuthInput';
import AsyncStorage from '@react-native-community/async-storage';

export default class Auth extends Component {
    state = {
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
        newUser: false,
    }

    signinOrSignup = () => {
        if (this.state.newUser) {
            this.signup()
        } else {
            this.signin()
        }
    }

    signup = async () => {
        try {
            await axios.post(`${server}/signup`, {
                name: this.state.name,
                email: this.state.email,
                password: this.state.password,
                confirmPassword: this.state.confirmPassword,
            })
            showSuccess('Usuário cadastrado!')
            this.setState({ newUser: false })
        }
        catch (e) {
            showError(e)
        }
    }

    signin = async () => {
        try {
            const res = await axios.post(`${server}/signin`, {
                email: this.state.email,
                password: this.state.password
            })
            AsyncStorage.setItem('userData', JSON.stringify(res.data))
            axios.defaults.headers.common['Authorization'] = `bearer ${res.data.token}`
            this.props.navigation.navigate('Home', res.data)
        } catch (e) {
            showError(e)
        }
    }

    render() {
        const validations = []
        validations.push(this.state.email && this.state.email.includes('@'))
        validations.push(this.state.password && this.state.password.length >= 6)

        if (this.state.newUser) {
            validations.push(this.state.name && this.state.name.trim().length >= 3)
            validations.push(this.state.password === this.state.confirmPassword)
        }

        const validForm = validations.reduce((total, atual) => total && atual)

        return (
            <ImageBackground source={BackgroundImage} style={styles.background}>
                <Text style={styles.title}>Tasks</Text>
                <View style={styles.formContainer}>
                    <Text style={styles.subTitle}>
                        {this.state.newUser ? 'Faça o seu cadastro' : 'Insira seus dados'}
                    </Text>
                    {this.state.newUser &&
                        <AuthInput icon="user" style={styles.input}
                            placeholder='Nome'
                            onChangeText={name => this.setState({ name })} />
                    }
                    <AuthInput icon="at" style={styles.input}
                        placeholder='E-mail'
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })} />
                    <AuthInput icon="lock" style={styles.input}
                        value={this.state.password}
                        placeholder='Senha'
                        onChangeText={password => this.setState({ password })}
                        secureTextEntry={true} />
                    {this.state.newUser &&
                        <AuthInput icon="lock" style={styles.input}
                            placeholder='Confirme a senha'
                            onChangeText={confirmPassword => this.setState({ confirmPassword })}
                            secureTextEntry={true} />
                    }
                    <TouchableOpacity onPress={this.signinOrSignup}
                        disabled={!validForm}>
                        <View style={[styles.button, validForm ? {} : { backgroundColor: '#AAA' }]} >
                            <Text style={styles.textButton}>
                                {this.state.newUser ? 'Cadastrar' : 'Entrar'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <TouchableWithoutFeedback style={{ paddin: 10 }} onPress={() => this.setState({ newUser: !this.state.newUser })}>
                    <Text style={styles.textButton}>
                        {this.state.newUser ? 'Já tenho login' : 'Faça seu cadastro'}
                    </Text>
                </TouchableWithoutFeedback>
            </ImageBackground >
        )
    }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
    },
    title: {
        fontFamily: GlobalStyle.fontFamily,
        color: GlobalStyle.color.secondary,
        fontSize: 70,
        marginBottom: 10
    },
    subTitle: {
        fontFamily: GlobalStyle.fontFamily,
        color: '#FFF',
        marginBottom: 10,
        fontSize: 20,
        textAlign: 'center'
    },
    formContainer: {
        backgroundColor: 'rgba(0,0,0,0.8)',
        padding: 20,
        width: '90%',
    },
    input: {
        backgroundColor: '#FFF',
        marginTop: 10
    },
    button: {
        backgroundColor: '#080',
        marginTop: 10,
        padding: 10,
        alignItems: 'center',
        borderRadius: 10
    },
    textButton: {
        fontFamily: GlobalStyle.fontFamily,
        color: '#FFF',
        fontSize: 20
    },
    newCad: {
        fontFamily: GlobalStyle.fontFamily,
        color: '#FFF'
    }
})