import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createDrawerNavigator } from 'react-navigation-drawer'

import Auth from './screens/Auth';
import TaskList from './screens/TaskList';

import Menu from './screens/Menu';
import common from './common'

import Loading from './screens/Loading'

const menuConfig = {
    initialRouteName: 'Today',
    contentComponent: Menu,
    contentOptions: {
        labelStyle: {
            fontFamily: common.fontFamily,
            fontWeight: 'normal',
            fontSize: 20
        },
        activeLabelStyle: {
            color: '#080',
            fontWeight: 'bold',
        }
    }
}

const menuRoutes = {
    Today: {
        name: 'Today',
        screen: props => <TaskList title="Hoje" daysAHead={0} {...props} />,
        navigationOptions: {
            title: 'Hoje',
        }
    },
    Tomorrow: {
        name: 'Tomorrow',
        screen: props => <TaskList title="Amanhã" daysAHead={1} {...props} />,
        navigationOptions: {
            title: 'Amanhã',
        }
    },
    Week: {
        name: 'Semana',
        screen: props => <TaskList title='Semana' daysAHead={7} {...props} />,
        navigationOptions: {
            title: 'Semana',
        }
    },
    Month: {
        name: 'Mês',
        screen: props => <TaskList title="Mês" daysAHead={30} {...props} />,
        navigationOptions: {
            title: 'Mês'
        }
    }
}

const menuNavigator = createDrawerNavigator(menuRoutes, menuConfig)
const mainRoutes = {
    Loading: {
        name: 'Loading',
        screen: Loading
    },
    Auth: {
        name: 'Auth',
        screen: Auth
    },
    Home: {
        name: 'Home',
        screen: menuNavigator
    }
}

const mainNavigator = createSwitchNavigator(mainRoutes, {
    initialRouteName: 'Loading'
})

export default createAppContainer(mainNavigator)