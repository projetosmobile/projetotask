export default {
    fontFamily: 'Lato',
    color: {
        today: '#B22222',
        tomorrow: '#FF8C00',
        week: '#008000',
        month: '#0000CD',
        secondary: '#FFF',
        mainText: '#363636',
        subText: '#696969'
    }
}