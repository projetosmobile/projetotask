import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import common from '../common';
import moment from 'moment';
import 'moment/locale/pt-br';
import Swipeable from 'react-native-gesture-handler/Swipeable';

export default props => {

    const doneOrNotStyle = props.doneAt != null ?
        { textDecorationLine: 'line-through' } : {}

    const date = props.estimateAt ? props.estimateAt : props.doneAt;
    const formatedDate = moment(date).locale('pt-br')
        .format('ddd, D [de] MMMM')

    const renderRight = () => {
        return <TouchableOpacity style={styles.right}
            onPress={() => props.onDelete && props.onDelete(props.id)}>
            <Icon name='trash' size={20} color='#FFF' />
        </TouchableOpacity>
    }
    const renderLeft = () => {
        return <View style={styles.left}>
            <Icon name='trash' size={20} color='#FFF' style={styles.iconExclude} />
            <Text style={styles.textExclude}>Excluir</Text>
        </View>
    }

    return (
        <Swipeable renderRightActions={renderRight}
            renderLeftActions={renderLeft}
            onSwipeableLeftOpen={() => props.onDelete && props.onDelete(props.id)}>
            <View style={styles.container}>
                <TouchableWithoutFeedback onPress={() => props.toggleTask(props.id)}>
                    <View style={styles.checkView}>
                        {getCheckView(props.doneAt)}
                    </View>
                </TouchableWithoutFeedback>
                <View>
                    <Text style={[styles.desc, doneOrNotStyle]}>{props.desc}</Text>
                    <Text style={styles.date}>{formatedDate}</Text>
                </View>
            </View>
        </Swipeable>
    )
}
function getCheckView(doneAt) {
    if (doneAt != null) {
        return (
            <View style={styles.done}>
                <Icon name='check' size={20}
                    color='white' />
            </View>
        )
    } else {
        return (
            <View style={styles.pending}>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderBottomColor: '#AAA',
        borderBottomWidth: 1,
        alignItems: 'center',
        paddingVertical: 10,
        backgroundColor: '#FFF'
    },
    checkView: {
        width: '20%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    done: {
        width: 25,
        height: 25,
        borderRadius: 13,
        backgroundColor: '#4D7030',
        alignItems: 'center',
        justifyContent: 'center'
    },
    pending: {
        width: 25,
        height: 25,
        borderRadius: 13,
        borderWidth: 1,
        borderColor: '#555',
    },
    desc: {
        fontFamily: common.fontFamily,
        color: common.color.mainText,
        fontSize: 15
    },
    date: {
        fontFamily: common.fontFamily,
        color: common.color.subText,
        fontSize: 12
    },
    right: {
        backgroundColor: 'red',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingHorizontal: 20
    },
    left: {
        flex: 1,
        backgroundColor: 'red',
        flexDirection: 'row',
        alignItems: 'center'
    },
    textExclude: {
        fontFamily: common.fontFamily,
        color: '#FFF',
        fontSize: 20,
        margin: 10
    },
    iconExclude: {
        marginLeft: 10
    }
})